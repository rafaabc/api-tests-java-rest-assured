[![pipeline status](https://gitlab.com/rafaabc/api-tests-java-rest-assured/badges/master/pipeline.svg)](https://gitlab.com/rafaabc/api-tests-java-rest-assured/-/commits/master)

# Framework of automated tests with Rest-Assured, Java, and Graddle

QA Auto Test Assignment

Framework to test in the API layer based
on the [Books API of New York Times](https://developer.nytimes.com/docs/books-product/1/overview)

# Pre-conditions

Tools:
- [IntelliJ IDEA](https://www.jetbrains.com/idea/download)
- [Java SE Development Kit 11](https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html)

Dependencies:
- [JUnit5](https://junit.org/junit5/docs/current/user-guide/#overview)
- [Rest-Assured](https://rest-assured.io/)

# Project Structure

```markdown
./project
├─ src/
    ├─ main
    └─ test/
        └─ java/
            ├─ config/
                └─ Configuration
            └─ isolated/
                ├─ TestBestSellerListAPI
                ├─ TestBestSellersHistoryEndpoint
                ├─ TestDateListEndpoint
                ├─ TestListsEndpoint
                ├─ TestNameEndpoint
                ├─ TestOverviewEndpoint
                └─ TestReviewsEndpoint
        └─ resources/
            └─ properties/
                └─ test.properties
```


- src: main dir
- main: dir with the base code
- java: default dir when the project is created
- config: dir with setup configurations
- Configuration: interface class to be extended in tests classes  
- test: package with test classes
- java: default dir when the project is created
- isolated: package with test classes

# CLI
Run all test classes through command line:

`./gradlew clean test`

As soon as the tests are executed, a folder will be created inside the "build" directory, called "reports".

# Report
Open the html test report that are stored after each test run:

`build -> reports -> tests -> test -> index.html`
