package isolated;

import config.Configuration;
import io.restassured.http.ContentType;
import org.aeonbits.owner.ConfigFactory;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

class TestListsEndpoint {

    private String token = "xDUMHspGUSuwgcXKKna27F6qsWj12NBm";

    @BeforeAll
    static void setUp() {
        Configuration configuration = ConfigFactory.create(Configuration.class);

        baseURI = configuration.baseURI();
        basePath = configuration.basePath();
    }

    @Test
    @DisplayName("When get all best-sellers, then a list of them is retrieved")
    void getGetAllBestSellers_ThenAListOfThemIsRetrieved() {

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("list", "hardcover-fiction")
        .when()
              .get("/lists.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(15));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all best-sellers without a token, then the status code is 401")
    void getGetAllBestSellersWithoutAToken_ThenTheStatusCodeIs401() {

        given()
            .contentType(ContentType.JSON)
            .queryParam("list", "hardcover-fiction")
        .when()
            .get("/lists.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all best-sellers without a list, then the status code is 400")
    void getGetAllBestSellersWithoutAList_ThenTheStatusCodeIs400() {

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
        .when()
            .get("/lists.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}
