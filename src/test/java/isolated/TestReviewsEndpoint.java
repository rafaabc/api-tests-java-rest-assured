package isolated;

import config.Configuration;
import io.restassured.http.ContentType;
import org.aeonbits.owner.ConfigFactory;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

class TestReviewsEndpoint {

    private String token = "xDUMHspGUSuwgcXKKna27F6qsWj12NBm";

    @BeforeAll
    static void setUp() {
        Configuration configuration = ConfigFactory.create(Configuration.class);

        baseURI = configuration.baseURI();
        basePath = configuration.basePath();
    }

    @Test
    @DisplayName("When get all book reviews by 10 digits isbn, then a filtered list of them is retrieved")
    void getAllBooksReviewsBy10DigitsISBN_ThenAFilteredListOfThemIsRetrieved() {

        long isbn10 = 1598536761L;

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("isbn", isbn10)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(1));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all book reviews by invalid 10 digits isbn, then th status code is 400")
    void getAllBooksReviewsByInvalid10DigitsISBN_ThenTheStatusCodeIs400() {

        long isbn10 = 11111111111L;

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("isbn", isbn10)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all book reviews by 13 digits isbn, then a filtered list of them is retrieved")
    void getAllBooksReviewsBy13DigitsISBN_ThenAFilteredListOfThemIsRetrieved() {

        long isbn13 = 9781598536768L;

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("isbn", isbn13)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(1));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all book reviews by invalid 13 digits isbn, then th status code is 400")
    void getAllBooksReviewsByInvalid13DigitsISBN_ThenTheStatusCodeIs400() {

        long isbn13 = 1111111111111L;

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("isbn", isbn13)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
            .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    @DisplayName("When get all book reviews by title, then a filtered list of them is retrieved")
    void getAllBooksReviewsByTitle_ThenAFilteredListOfThemIsRetrieved() {

        String title = "THE MAN WHO LIVED UNDERGROUND";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("title", title)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(1));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all book reviews by invalid title, then the status code is 400")
    void getAllBooksReviewsByInvalidTitle_ThenTheStatusCodeIs400() {

        String title = "";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("title", title)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    @DisplayName("When get all book reviews by author, then a filtered list of them is retrieved")
    void getAllBooksReviewsByAuthor_ThenAFilteredListOfThemIsRetrieved() {

        String author = "Richard Wright";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("author", author)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("status", equalTo("OK"))
                .body("copyright", equalTo("Copyright (c) 2021 The New York Times Company.  All Rights Reserved."))
                .body("num_results", equalTo(1));
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all book reviews by invalid author, then the status code is 400")
    void getAllBooksReviewsByInvalidAuthor_ThenTheStatusCodeIs400() {

        String author = "";

        given()
            .contentType(ContentType.JSON)
            .queryParam("api-key", token)
            .queryParam("author", author)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Disabled("Skipped")
    @Test
    @DisplayName("When get all book reviews without a token, then the status code is 401")
    void getAllBooksReviewsByAuthor_ThenTheStatusCodeIs401() {

        String author = "Richard Wright";

        given()
            .contentType(ContentType.JSON)
            .queryParam("author", author)
        .when()
            .get("/reviews.json")
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }
}
